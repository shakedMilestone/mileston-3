﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;

namespace BL
{
    public interface IBL
    {

        void add(Doctor d);
        void add(Patient p);
        void add(Visit v);
        void add(Treatment t);
        bool chkDate(string str);


        void remove(char db, int ID);
        void changePass(LoginUser currUser, string newPass);
        Patient matchUsrToPatient(int ID, char type);
        Doctor matchUsrToDoctor(int ID, char type);
        Visit findVisitByID(int id, Patient p);
        Treatment findTreatmentByID(int tid, Visit v);


       // id
        void updateDoctor(int doctorID, String field, String updateField);
        void updatePatient(int patientID, String field, String updateField);

        Patient findPatientByID(int PatId);     
        Doctor findDoctorByID(int DocId);

        List<Doctor> getDoctorBy( String catagory, String info);

        //  {main doctor, age, gender}
        List<Patient> getPatientBy(String catagory, String info);
        String getPatientsList(int doctorID);
        List<Visit> visitPerPatient(int PatientID);
        List<Visit> getVisitBy(String catagory, String info);
        List<Treatment> getTreatmentBy(String catagory, String info);
        char checkAuth(string usrname, string pass);
        LoginUser findUser(string usrName);
    }
}
