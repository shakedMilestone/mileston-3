﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;
using DAL;


namespace BL
{
    public class Milestone_BL : IBL
    {
        IDAL itsDAL;

        public Milestone_BL(IDAL dal)
        {
            itsDAL = dal;
        }

        public bool chkDate(string str)
        {
            bool ans = true;
            int i = 0;
            if (str.Length > 8)
                ans = false;
            while (i < 8 && ans == true)
            {
                if ((str[i] > '9' && str[i] < '0') && i != 2 && i != 5)
                {
                    ans = false;
                }
                if (i == 2 || i == 5)
                {
                    if (str[i] != '/')
                        ans = false;
                }
                i++;
            }
            return ans;
        }

        public Visit findVisitByID(int id,Patient p)
        {
            return itsDAL.findVisitByID(id,p);
        }

        public Treatment findTreatmentByID(int tid, Visit v)
        {
            return itsDAL.findTreatmentByID(tid, v);

        }

        public Doctor matchUsrToDoctor(int ID, char type)
        {
            if ('d' == type)
            {
                Doctor d = itsDAL.findDoctorByID(ID);
                return d;
            }
            else return null;
        }

        public Patient matchUsrToPatient(int ID, char type)
        {
            if ('p' == type)
            {
                Patient p = itsDAL.findPatientByID(ID);
                return p;
            }
            else return null;
        }

        public void changePass(LoginUser currUser, string newPass)
        {
            itsDAL.changePass(currUser,newPass);
        }

        public char checkAuth(string usrname, string pass)
        {
            char ans = 'N';
            LoginUser tmpUsr = itsDAL.findUser(usrname);
            if (tmpUsr != null)
                if (tmpUsr.getpass() == pass)
                    ans = tmpUsr.getUsrType();
            return ans;
        }

        /**
         * ADD Methods
        **/

        /*
         * Add doctor to list
         **/

        public List<Visit> getVisitBy(String catagory, String info)
        {
            return itsDAL.getVisitBy(catagory, info);
        }

        public List<Treatment> getTreatmentBy(String catagory, String info)
        {
            return itsDAL.getTreatmentBy(catagory, info);
        }

        public void add(BL_Backend.Doctor d) {
            if (itsDAL.findDoctorByID(d.ID)!=null)
            {
               //need to add print to consol. 
            }
            else
            {
                itsDAL.add(d);
            }
        }

        /*
         * Add Patient to list
         * Add patient to doctor list 
         **/
        public void add(BL_Backend.Patient p)
        {
            if (itsDAL.findPatientByID(p.ID)!=null)
            {
                //need to add print to consol.
            }
            else
            {
                itsDAL.add(p);
                //add patient to doctor list
                {
                    itsDAL.updateDoctor(p.mainDoctorID, "patients", p.ID.ToString());                 
                 }
            }
        }

      /*
      * Add Visit to list
        **/
        public void add(BL_Backend.Visit v)
        {
           itsDAL.add(v);
        }

      /*
      *Add treatment to list
      **/
        public void add(BL_Backend.Treatment t)
        {
            itsDAL.add(t);
        }

        /**
         * Remove Methods
        **/
        public void remove(char db, int ID)
        {
            if (db.Equals('d'))
            {
                itsDAL.removeDoctor(ID);
            }

            if (db.Equals('p'))
            {
                // remove patient from doctor list
                itsDAL.removePatient(ID);
            }
        }

        /**
         * UPDATE Methods
        **/

        public void updateDoctor(int doctorID, String field, String updateField)
        {
            itsDAL.updateDoctor(doctorID, field, updateField);
        }

        public void updatePatient(int patientID, String field, String updateField)
        { 
            itsDAL.updatePatient(patientID, field, updateField);
        }

       public Patient findPatientByID(int PatId)
       {
           return itsDAL.findPatientByID(PatId);
       }

       public Doctor findDoctorByID(int DocId)
       {
           //Console.Write("this is the docs tostring inside BL " + itsDAL.findDoctorByID(DocId));
           return itsDAL.findDoctorByID(DocId);
       }

       public List<Doctor> getDoctorBy(String catagory, String info)
       {
           return itsDAL.getDoctorBy(catagory, info);
       }

       public List<Patient> getPatientBy(String catagory, String info)
       {
           return itsDAL.getPatientBy(catagory, info);
       }

       public String getPatientsList(int doctorID)
       {
           if (itsDAL.findDoctorByID(doctorID) != null)
           {
               //need to add print to consol. 
               return itsDAL.getPatientsList(doctorID);
           }
           else
           {
               return null;
           }
       }

       public LoginUser findUser(string usrName)
       {
          return itsDAL.findUser(usrName);
       }

       public List<Visit> visitPerPatient(int PatientID)
       {
           if (itsDAL.findPatientByID(PatientID) != null)
           {
               //need to add print to consol- no such patient
               return itsDAL.visitPerPatient(PatientID);
           }
           else
           {
               return null;
           }
       }
    }
}
