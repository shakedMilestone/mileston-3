﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using DAL;
using BL_Backend;

namespace LINQ_DAL_tests
{
    [TestFixture]
    public class tests
    {

        LINQ_DAL newDL = new LINQ_DAL();

        [Test]
        public void addNewPatient_test()
        {
            Patient newPatient = new Patient("shaked", "krigel", 0211, 123, 'F', 22);
            newDL.add(newPatient);
           Assert.IsTrue(newDL.DBOfPatient.Contains(newPatient));
     
        }

        [Test]
        public void updateDoctor_test()
        {
            Doctor newDoctor = new Doctor("shani", "levi", 2030, 3000.0, 'F', "");
            newDL.add(newDoctor);
            newDL.updateDoctor(2030, "first-name", "shaked");
            Assert.AreEqual(newDoctor.firstName, "shaked");
            Assert.AreNotEqual(newDoctor.firstName, "shani");
        }

        [Test]
        public void addVisit_test()
        {
            Patient newPatient = new Patient("shaked", "krigel", 0211, 123, 'F', 22);
            newDL.add(newPatient);
            Visit newVisit = new Visit("1.1.11", 123, 0211, "BLA-BLA-BLA");
          Assert.AreEqual(newPatient.visits.Count(), 0);
            newDL.add(newVisit);
           Assert.AreEqual(newPatient.visits.Count(), 1);
            
        }


        [Test]
        public void loginUser_test()
        {
            Patient newPatient = new Patient("shaked", "krigel", 0211, 123, 'F', 22);
            LoginUser user = new LoginUser("shaked", "shaked", 'p', 0211);
            newDL.add(user);
            Assert.IsNotNull ( newDL.findUser ("shaked") );
            Assert.AreEqual(newDL.findUser("shaked").getpass(), "shaked");
        }

        [Test]
        public void removePatintFromDoctorList_test()
        {
            Doctor newDoctor = new Doctor("shani", "levi", 2030, 3000.0, 'F', "");
            Patient newPatient = new Patient("shaked", "krigel", 0211, 2030, 'F', 22);
            Assert.IsEmpty(newDoctor.patientID);
            newDL.add(newPatient);
            Assert.IsTrue(newDoctor.patientID.Length == 0);

        }



    }
}
