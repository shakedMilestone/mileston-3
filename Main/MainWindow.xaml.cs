﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BL;
using BL_Backend;
using DAL;
using MCCSYS;
namespace Main
{
    public partial class MainWindow : Window
    {
        IDAL aDAL;
        IBL aBL;
        /*Source="http://oi59.tinypic.com/25a54pl.jpg"*/
        public MainWindow()
        {
            InitializeComponent();
            aDAL = new LINQ_DAL();
            aBL = new Milestone_BL(aDAL);
        }

        private void login_Click(object sender, EventArgs e)
        {
            string usrName = username.Text;
            string pass = password.Password;
            LoginUser curr = aBL.findUser(usrName);
            Patient currPat;
            char tmpType;
            int tmpid;
            if (curr != null)
            {
                tmpType = curr.getUsrType();
                tmpid = curr.getId();
            }
            else
            {
                tmpType = 'N';
                tmpid = -1;
            }
            Patient_Profile p;
            char access = aBL.checkAuth(usrName, pass);
            if (access == 'N')
            {
                MessageBox.Show("Username or Password is incorrect..");
            }
            else
            {
                if (curr.getUsrType() == 'p')
                {
                    currPat = aBL.matchUsrToPatient(tmpid, tmpType);
                    p = new Patient_Profile(curr, currPat, aBL);
                    p.Show();
                }
                if (curr.getUsrType() == 'd')
                {
                    welcome_doctor wd = new welcome_doctor(aBL, curr);
                    wd.Show();
                }
                if (curr.getUsrType() == 'a')
                {
                    welcome_admin wa = new welcome_admin(aBL, curr);
                    wa.Show();
                }
            }
        }
    }
}