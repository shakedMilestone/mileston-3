﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;


namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for welcome_doctor.xaml
    /// </summary>
    public partial class welcome_doctor : Window
    {
        IBL aBL;
        LoginUser user;

        public welcome_doctor(IBL aBL, LoginUser user)
        {
            InitializeComponent();
            this.aBL = aBL;
            this.user = user;
        }

        private void setting_Click(object sender, RoutedEventArgs e)
        {
            Settings set = new Settings(user, aBL);
            set.Show();
        }

        private void search_Click(object sender, RoutedEventArgs e)
        {
            Patient_Search ps = new Patient_Search(aBL, user.getId());
            ps.Show();
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            addMenuForDoctor addMenuPatient = new addMenuForDoctor(user.getId(), aBL);
            addMenuPatient.Show();
        }

       
    }
}
