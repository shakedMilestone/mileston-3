﻿using System;
using System.Collections.Generic;
using System.Linq;

using BL;
using BL_Backend;


namespace Helper
{
    public class HelperForPrint : Ihelper
    {
        IBL bl;

        public HelperForPrint(IBL abl)
        {
            this.bl = abl;
        }

        private void DisplayDoctorResult(List<Doctor> doctors)
        {
            foreach (Doctor doc in doctors)
            {
                Console.Write(doc);
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");
        }

        private void DisplayPatientResult(List<Patient> patients)
        {
            foreach (Patient patient in patients)
            {
                Console.Write(patient); 
                Console.WriteLine("\n");
            }
            Console.WriteLine("\n");

        }

        public string DisplayVisitResult(List<Visit> visits)
        {
            string ans = "";
            foreach (Visit visit in visits)
            {
                ans += visit;
            }
            ans += "\n---------------------\n";
            return ans;
        }

        public string DisplayTreatmentResult(List<Treatment> treatments)
        {
            string ans = "";
            foreach (Treatment treat in treatments)
            {
                ans += treat;
                ans += "\n*******\n";
            }
            return ans;
        }

       /* public void Run()
        {
            while (true)
            {
                Console.Write("Welcome, what DB do you want to access?\n1-Doctors\n2-Patients\n3-Visits\n4-Treatments\n");
                char c = Console.ReadKey().KeyChar;
                int asciiNumber = c;
                if (asciiNumber != 'q' && asciiNumber != 'Q')
                {
                    handleDB(asciiNumber);
                }
                if (asciiNumber == 'q' || asciiNumber == 'Q')
                {
                    Console.Write("Bye..\n");
                    break;
                }
            }
        }

        void handleDB(int asciiNumber)
        {
            Console.Write("\n");
            char db = '/';
            bool rightMenu = false;
            if (asciiNumber == '1')        //Doctors
            {
                db = 'd';
                rightMenu = true;
            }
            else if (asciiNumber == '2')   //Patients
            {
                db = 'p';
                rightMenu = true;
            }
            else if (asciiNumber == '3')   //Visits
            {
                db = 'v';
                rightMenu = true;
            }
            else if (asciiNumber == '4')   //Treatments
            {
                db = 't';
                rightMenu = true;
            }
            if (!rightMenu)
            {
                Console.Write("No such option please retype..\n");
                return;
            }
            else
            {
                Console.Write("Choose your action:\n 1)Add\n 2)Remove/Update\n 3)Display\n");
                char c = Console.ReadKey().KeyChar;
                int actioNum = c;
                if (c == '1' || c == '2' || c == '3')
                {
                    hanldeAction(actioNum, db);
                }
                else
                {
                    Console.Write("\nNo such option..\n");
                }
            }

        }

        void hanldeAction(int actioNum, char c)
        {
            if (actioNum == '1'){
                addToDB(c);
            }
            else if (actioNum == '2')          /*remove/update*//*
            {
                updRem(c);
            }
            else if (actioNum == '3')         /*display*//*
            {
                display(c);
            }
        }

        void updRem(char db)
        {
            if (db != 'v' && db != 't')
            {
                if (db == 'd')
                {
                    updRemDoc(db);
                }
                else if (db == 'p')
                {
                    updRemPat(db);
                }
            }
            else
            {
                Console.Write("\nYou can't update Visits or Treatments..\n\n");
            }
        }

        void updRemDoc(char db)
        {
            Console.Write("\nEnter the ID:\n");/*doctor*//*
            bool validID = false;
            int id = -1;
            while (!validID)
            {
                try
                {
                    String inID = Console.ReadLine();
                    id = Convert.ToInt32(inID);
                    validID = true;
                }
                catch
                {
                    Console.Write("\nNot a valid ID..\n");
                    validID = false;
                }
            }
            Console.Write("Choose your action:\n 1)Update\n 2)Remove\n");
            int actioNum = Console.ReadKey().KeyChar;
            Console.Write("\n");
            if (actioNum == '1' || actioNum == '2')
            {
                switch (actioNum)
                {
                    case '1':                         /*update*//*
                        Console.Write("Choose catagory to update: \n");
                        String cat = "";
                        Console.Write(" 1)First-name\n 2)Last-name\n 3)Salary\n 4)Gender \n");
                        int choice = Console.ReadKey().KeyChar;
                        if (choice == '1')
                        {
                            cat = "first-name";
                        }
                        if (choice == '2')
                        {
                            cat = "last-name";
                        }
                        if (choice == '3')
                        {
                            cat = "salary";
                        }
                        if (choice == '4')
                        {
                            cat = "gender";
                        }
                        try
                        {
                            Console.Write("\nInsert data for update\n");
                            String info = Console.ReadLine();
                            bl.updateDoctor(id, cat, info);
                            Console.Write("...data UPDATED...\n\n");
                        }
                        catch
                        {
                            Console.Write("\nNo such option..\n");
                        }
                        break;
                    case '2':                 /*remove*//*
                        if (bl.findDoctorByID(id) != null)
                        {
                            bl.remove(db, id);
                            Console.Write("\n...Removed...\n");
                            break;
                        }
                        else
                        {
                            Console.Write("No such Doctor..\n\n");
                            break;
                        }
                }
            }
            else
            {
                Console.Write("\nNo such option..\n");
            }
        }

        void updRemPat(char db)
        {
            Console.Write("\nEnter the ID\n");
            String inID = Console.ReadLine();
            int id = Convert.ToInt32(inID);
            Console.Write("Choose your action:\n 1)Update\n 2)Remove\n");
            int actioNum = Console.ReadKey().KeyChar;
            if (actioNum == '1' || actioNum == '2')
            {
                switch (actioNum)
                {
                    case '1':
                        Console.Write("\nChoose catagory to update: \n");
                        String cat = "";
                        Console.Write(" 1)First-name\n 2)Last-name\n 3)Main-doc-id\n 4)Gender\n 5)Age\n");
                        int choice = Console.ReadKey().KeyChar;
                        switch (choice)
                        {
                            case '1':
                                cat = "first-name";
                                break;
                            case '2':
                                cat = "last-name";
                                break;
                            case '3':
                                cat = "main-doc-id";
                                break;
                            case '4':
                                cat = "gender";
                                break;
                            case '5':
                                cat = "age";
                                break;
                        }
                        try
                        {
                            Console.Write("\nInsert data for update\n");
                            String info = Console.ReadLine();
                            bl.updatePatient(id, cat, info);
                            Console.Write("...data UPDATED...\n\n");
                        }
                        catch
                        {
                            Console.Write("No such patient..\n");
                        }
                        break;
                    case '2':                 /*remove*//*
                        if (bl.findPatientByID(id) != null)
                        {
                            bl.remove(db, id);
                            Console.Write("\n...Removed...\n");
                            break;
                        }
                        else
                        {
                            Console.Write("\nNo such patient..\n\n");
                            break;
                        }
                }
            }
            else
            {
                Console.Write("\nNo such option..\n");
            }
        }

        void addToDB(char db)
        {
            Console.Write("\nEnter the data you want to add, while keeeping the next format: \n");
            switch (db)
            {
                case 'd':
                    addDoctor();
                    break;
                case 'p':
                    addPatient();
                    break;
                case 'v':
                    addVisit();
                    break;
                case 't':
                    addTreatment();
                    break;
            }
        }

        void addDoctor()
        {
            Console.Write("first-name,last-name,ID,salary,gender,<patientId1,patientId2,...>\n");
            String toAdd = Console.ReadLine();
            int i = 0;
            String word = "";
            int arrIndx = 0;
            String[] toBuild = new String[6];
            while (i < toAdd.Length)
            {
                if (toAdd[i] == ',')
                {
                    toBuild[arrIndx] = word;
                    arrIndx++;
                    toAdd.Substring(i);
                    word = "";
                    i++;
                }
                if (toAdd[i] == '<')
                {
                    word = toAdd.Substring(i + 1, toAdd.Length - i - 2);
                    toBuild[arrIndx] = word;
                    break;
                }
                word += toAdd[i];
                i++;
            }
            try
            {
                BL_Backend.Doctor newDoc = new BL_Backend.Doctor(toBuild[0], toBuild[1], Convert.ToInt32(toBuild[2]), double.Parse(toBuild[3]), toBuild[4][0], toBuild[5]);
                bl.add(newDoc);
                Console.Write("...Added...\n\n");
            }
            catch
            {
                Console.Write("Input Error\n\n");
            }
        }

        void addPatient()
        {

            Console.Write("first-name,last-name,ID,main-doc-id,gender,age\n");
            String toAdd = Console.ReadLine();
            int i = 0;
            String word = "";
            int arrIndx = 0;
            String[] toBuild = new String[6];
            int sumOfPsiks = 0;
            foreach (char psik in toAdd)
            {
                if (psik == ',')
                    sumOfPsiks++;
            }
            int countPsik = 5;
            if (sumOfPsiks == countPsik)
            {
                while (i <= toAdd.Length)
                {
                    if (countPsik > 0 && (toAdd[i] == ','))
                    {
                        countPsik--;
                        toBuild[arrIndx] = word;
                        arrIndx++;
                        word = "";
                        i++;
                        toAdd.Substring(i);
                    }
                    if (i < toAdd.Length)
                    {
                        word += toAdd[i];
                    }
                    i++;
                }
                toBuild[arrIndx] = word;
            }
            try
            {
                BL_Backend.Patient newPat = new BL_Backend.Patient(toBuild[0], toBuild[1],
                    Convert.ToInt32(toBuild[2]), Convert.ToInt32(toBuild[3]), toBuild[4][0], Convert.ToInt32(toBuild[5]));
                bl.add(newPat);
                if (bl.findDoctorByID(Convert.ToInt32(toBuild[3])) != null)
                {
                    Console.Write("...Added...\n\n");
                }
                else
                {
                    Console.Write("No such doctor exists..\n");
                }
            }
            catch
            {
                Console.Write("Input Error\n\n");
            }
        }

        void addVisit()
        {

            Console.Write("date-of-visit(DD/MM/YY),assigned-doctor-id,patient-id,doctor-notes\n");
            String toAdd = Console.ReadLine();
            int i = 0;
            String word = "";
            int arrIndx = 0;
            String[] toBuild = new String[4];
            int sumOfPsiks = 0;
            foreach (char psik in toAdd)
            {
                if (psik == ',')
                    sumOfPsiks++;
            }
            int countPsik = 3;
            if (sumOfPsiks == countPsik)
            {
                while (i <= toAdd.Length)
                {
                    if (countPsik > 0 && (toAdd[i] == ','))
                    {
                        countPsik--;
                        toBuild[arrIndx] = word;
                        arrIndx++;
                        word = "";
                        i++;
                        toAdd.Substring(i);
                    }
                    if (i < toAdd.Length)
                    {
                        word += toAdd[i];
                    }
                    i++;
                }
                toBuild[arrIndx] = word;
            }
            try
            {
                BL_Backend.Visit newVisit = new BL_Backend.Visit(toBuild[0], Convert.ToInt32(toBuild[1]), Convert.ToInt32(toBuild[2]), toBuild[3]);
                bl.add(newVisit);
                Console.Write("...Added...\n\n");
            }
            catch
            {
                Console.Write("Input Error\n\n");
            }
        }

        void addTreatment()
        {

            Console.Write("start-date(DD/M/YY),finish-date,assigned-doctor-id,prognosis,\nprescriptions,visit-id\n");
            String toAdd = Console.ReadLine();
            int i = 0;
            String word = "";
            int arrIndx = 0;
            String[] toBuild = new String[6];
            int sumOfPsiks = 0;
            foreach (char psik in toAdd)
            {
                if (psik == ',')
                    sumOfPsiks++;
            }
            int countPsik = 5;
            int originalcountPsik = sumOfPsiks;
            if (sumOfPsiks == countPsik)
            {
                while (i <= toAdd.Length)
                {
                    if (countPsik > 0 && (toAdd[i] == ','))
                    {
                        countPsik--;
                        toBuild[arrIndx] = word;
                        arrIndx++;
                        word = "";
                        i++;
                        toAdd.Substring(i);
                    }
                    if (i < toAdd.Length)
                    {
                        word += toAdd[i];
                    }
                    i++;
                }
                toBuild[arrIndx] = word;
            }
            try
            {
                if (sumOfPsiks == originalcountPsik)
                {
                    BL_Backend.Treatment newTreatment = new BL_Backend.Treatment(toBuild[0], toBuild[1],
                        Convert.ToInt32(toBuild[2]), toBuild[3], toBuild[4], Convert.ToInt32(toBuild[5]));
                    bl.add(newTreatment);
                    Console.Write("...Added...\n\n");
                }
                else
                {
                    Console.Write("Input Error\n\n");
                }
            }
            catch
            {
                Console.Write("Input Error\n\n");
            }
        }

        void display(char db)
        {
            Console.Write("\nDisplay by:\n 1)ID\n 2)Else\n");
            int actioNum = Console.ReadKey().KeyChar;
            if (actioNum == '1' || actioNum == '2')
            {
                switch (actioNum)
                {
                    case '1':                     /*ID*//*
                        displayByID(db);
                        break;
                    case '2':                     /*Not ID*//*
                        dispayNotByID(db);
                        break;
                }
            }
            else
            {
                Console.Write("\nNo such option..\n\n");
            }
        }

        void dispayNotByID(char db)
        {
            switch (db)
            {
                case 'd':
                    displayDoctorNotByID();
                    break;
                case 'p':
                    displayPatientNotByID();
                    break;
                case 'v':
                    displayVisits();
                    break;
                case 't':
                    displayTreatments();
                    break;
            }
        }

        void displayByID(char db)
        {
            if (db != 'v' && db != 't')
            {
                if (db == 'd')
                {
                    Console.Write("\nInsert ID:\n");
                    String searchID = Console.ReadLine();
                    int id = Convert.ToInt32(searchID);
                    if (bl.findDoctorByID(id) != null)
                    {
                        Console.Write(bl.findDoctorByID(id));
                        Console.Write("\n");
                    }
                    else
                    {
                        Console.Write("No doctor found..\n");
                    }
                    Console.Write("\n");
                }
                if (db == 'p')
                {
                    Console.Write("\nInsert ID:\n");
                    String searchID = Console.ReadLine();
                    int id = Convert.ToInt32(searchID);
                    if (bl.findPatientByID(id) != null)
                    {
                        Console.Write(bl.findPatientByID(id));
                        Console.Write("\n");
                    }
                    else
                    {
                        Console.Write("No patient found..\n");
                    }
                    Console.Write("\n");
                }
               /* break; *//*
            }
            else
            {
                Console.Write("\nYou cant display Visits and Treatments by ID\n\n");
            }
        }

        void displayDoctorNotByID()
        {
            Console.Write("Choose catagory for search: \n");
            String cat = "";
            String info = "";
            Console.Write(" 1)First-name\n 2)Last-name\n");
            int choice = Console.ReadKey().KeyChar;
            Console.Write("\n");
            switch (choice)
            {
                case '1':
                    cat = "first-name";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
                case '2':
                    cat = "last-name";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
            }
            List<Doctor> doctorResault = bl.getDoctorBy(cat, info);
            DisplayDoctorResult(doctorResault);
        }

        void displayPatientNotByID()
        {
            Console.Write("Choose catagory for search: \n");
            String cat = "";
            String info = "";
            Console.Write(" 1)First-name\n 2)Last-name\n 3)Main-doc-id\n 4)Gender\n 5)Age\n");
            int choice = Console.ReadKey().KeyChar;
            Console.Write("\n");
            switch (choice)
            {
                case '1':
                    cat = "first-name";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    Console.Write("\n");
                    break;
                case '2':
                    cat = "last-name";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    Console.Write("\n");
                    break;
                case '3':
                    cat = "main-doc-id";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
                case '4':
                    cat = "gender";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    Console.Write("\n");
                    break;
                case '5':
                    cat = "age";
                    Console.Write("\n");
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
            }
            List<Patient> patientResault = bl.getPatientBy(cat, info);
            DisplayPatientResult(patientResault);
        }

        void displayVisits()
        {
            Console.Write("Choose catagory for search: \n");
            String cat = "";
            String info = "";
            Console.Write(" 1)Date-of-visit(DD/MM/YY)\n 2)Assigned-doctor-id\n 3)Patient-id\n");
            int choice = Console.ReadKey().KeyChar;
            Console.Write("\n");
            switch (choice)
            {
                case '1':
                    cat = "date-of-visit";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
                case '2':
                    cat = "assigned-doctor-id";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
                case '3':
                    cat = "patient-id";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
            }
            List<Visit> visitResault = bl.getVisitBy(cat, info);
            DisplayVisitResult(visitResault);
        }

        void displayTreatments()
        {
            Console.Write("Choose catagory for search: \n");
            String cat = "";
            String info = "";
            Console.Write(" 1)Start-date(DD/M/YY)\n 2)Finish-date\n 3)Assigned-doctor-id\n");
            int choice = Console.ReadKey().KeyChar;
            Console.Write("\n");
            switch (choice)
            {
                case '1':
                    cat = "start-date";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
                case '2':
                    cat = "finish-date";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
                case '3':
                    cat = "assigned-doctor-id";
                    Console.Write("\nInsert info to search by:\n");
                    info = Console.ReadLine();
                    break;
            }
            List<Treatment> treatmentResault = bl.getTreatmentBy(cat, info);
            DisplayTreatmentResult(treatmentResault);
        }*/
    }
}


