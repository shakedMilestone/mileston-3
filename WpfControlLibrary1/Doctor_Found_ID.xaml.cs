﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;

using BL_Backend;


namespace MCCSYS
{
    public partial class Doctor_Found_ID : Window
    {
       
        IBL bl;
        Doctor curr;

        public Doctor_Found_ID(Doctor currdoc, IBL abl)
        {
            InitializeComponent();
          
            bl = abl;
            curr = currdoc;
            First.Text = currdoc.firstName;
            Last.Text = currdoc.lastName;
            Gender.Text = currdoc.getGender().ToString();
            Patients.Text = currdoc.patientID;
            Patients.IsReadOnly = true;
            Gender.IsReadOnly = true;
        }

        private void update(object sender, RoutedEventArgs e)
        {
            bl.updateDoctor(curr.ID, "first-name", First.Text);
            bl.updateDoctor(curr.ID, "last-name", Last.Text);
            MessageBox.Show("Doctor updated!");
            this.Close();
        }

        private void remove(object sender, RoutedEventArgs e)
        {
            bl.remove('d', curr.ID);
            MessageBox.Show("Doctor removed!");
            this.Close();
        }
    }
}