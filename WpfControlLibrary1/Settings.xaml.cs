﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;


namespace MCCSYS
{
    public partial class Settings : Window
    {
        LoginUser curr;
        IBL aBL;

        public Settings(LoginUser currUser,IBL bl)
        {
            InitializeComponent();
            curr = currUser;
            aBL = bl;
        }

        private void done_Click(object sender, RoutedEventArgs e)
        {
            string newPass = newPassword.Text;
            if (newPass != "")
            {
                aBL.changePass(curr, newPass);
                MessageBox.Show("Password changed..");
                this.Close();
            }
            else
            {
                MessageBox.Show("No password inserted");
            }
        }
    }
}


/* call settings window:

                    Settings settings = new Settings(curr);
                    settings.Show();
*/