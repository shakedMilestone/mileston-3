﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Helper;
using BL;
using BL_Backend;

namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for Visit_change.xaml
    /// </summary>
    public partial class Treatment_change : Window
    {
        Ihelper helper;
        IBL bl;
        Patient curr;

        public Treatment_change(Patient currPat, IBL abl)
        {
            InitializeComponent();
            curr = currPat;
            helper = new HelperForPrint(abl);
            bl = abl;
            visitID.IsEnabled = true;
            treatmentID.IsEnabled = true;
            Date_of_start.IsEnabled = false;
            End_Date.IsEnabled = false;
            Prescriptions.IsEnabled = false;
            Prognosis.IsEnabled = false;
        }

        private void update(object sender, RoutedEventArgs e)
        {
            Treatment t;
            Visit v = bl.findVisitByID(Convert.ToInt32(visitID.Text), curr);
            int tid;
            int.TryParse(treatmentID.Text, out tid);
            if (v != null)
            {
                t = bl.findTreatmentByID(tid, v);
                if (t != null)
                {
                    String newEnDate = End_Date.Text;
                    if (bl.chkDate(newEnDate) == true)
                    {
                        String newPrescriptions = Prescriptions.Text;
                        String newPrognosis = Prognosis.Text;
                        t.dateOfFinish = newEnDate;
                        t.Prescriptions = newPrescriptions;
                        t.Prognosis = newPrognosis;
                        MessageBox.Show("Treatment was updated!");
                        this.Close();
                    }
                    else MessageBox.Show("Date should be DD/MM/YY");
                }
            }
        }

        private void search(object sender, RoutedEventArgs e)
        {
            Treatment t;
            Visit v = bl.findVisitByID(Convert.ToInt32(visitID.Text), curr);
            int tid;
            int.TryParse(treatmentID.Text, out tid);
            if ( v!=null)
            {
                t = bl.findTreatmentByID(tid, v);
                if (t != null)
                {
                    Date_of_start.Text = t.dateOfStart;
                    End_Date.Text = t.dateOfFinish;
                    Prescriptions.Text = t.Prescriptions;
                    Prognosis.Text = t.Prognosis;
                    visitID.IsReadOnly = true;
                    treatmentID.IsReadOnly = true;
                    Date_of_start.IsReadOnly = true;
                    End_Date.IsEnabled = true;
                    Prescriptions.IsEnabled = true;
                    Prognosis.IsEnabled = true;
                }
                else MessageBox.Show("No such treatment found");
            }
            else MessageBox.Show("No such visit exist!");
        }

    }
}
