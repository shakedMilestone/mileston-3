﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Helper;
using BL;
using BL_Backend;

namespace MCCSYS
{

    public partial class Patient_Search : Window
    {
        Ihelper helper;
        IBL abl;
        int mDocID;

        public Patient_Search(IBL mainBL,int mainDocId)
        {
            helper = new HelperForPrint(mainBL);
            abl = mainBL;
            mDocID = mainDocId;
            InitializeComponent();
            AgeText.IsEnabled = false;
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void Age_Checked(object sender, RoutedEventArgs e)
        {
            AgeText.IsEnabled = true;
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void Gender_Checked(object sender, RoutedEventArgs e)
        {
            GenderText.IsEnabled = true;
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
            AgeText.IsEnabled = false;
        }

        private void Last_Checked(object sender, RoutedEventArgs e)
        {
            LastText.IsEnabled = true;
            FirstText.IsEnabled = false;
            AgeText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void First_Checked(object sender, RoutedEventArgs e)
        {
            FirstText.IsEnabled = true;
            AgeText.IsEnabled = false;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void ID_Checked(object sender, RoutedEventArgs e)
        {
            IDText.IsEnabled = true;
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            AgeText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void search(object sender, RoutedEventArgs e)
        {
            if (Age.IsChecked == true)
            {
                string ageSearch = AgeText.Text;
                if (ageSearch != "")
                    searchNotID(ageSearch, "age");
                else MessageBox.Show("No data was entered!");
            }
            if (Gender.IsChecked == true)
            {
                string genderSearch = GenderText.Text;
                if (genderSearch != "")
                    searchNotID(genderSearch, "gender");
                else MessageBox.Show("No data was entered!");
            }
            if (First.IsChecked == true)
            {
                string firstSearch = FirstText.Text;
                if (firstSearch != "")
                    searchNotID(firstSearch, "first-name");
                else MessageBox.Show("No data was entered!");
            }
            if (Last.IsChecked == true)
            {
                string lastSearch = LastText.Text;
                if (lastSearch != "")
                    searchNotID(lastSearch, "last-name");
                else MessageBox.Show("No data was entered!");
            }
            if (ID.IsChecked == true)
            {
                string idSearch = IDText.Text;
                searchID(idSearch);
            }
        }

        public void searchNotID(string info, string catagory)
        {
            string str="";
            List<Patient> patients = abl.getPatientBy(catagory, info); ;
            foreach (Patient patient in patients)
            {
                if (patient.mainDoctorID == mDocID)
                    str += patient + "\n";
                if(mDocID == -1)
                    str += patient + "\n";
            }
            Patient_Found pf = new Patient_Found(str);
            pf.Show();
        }
        
        public void searchID(string id)
        {
            int idvalue;
            int.TryParse(id,out idvalue);
            if (abl.findPatientByID(idvalue) != null)
            {
                Patient p = abl.findPatientByID(idvalue);
                if (p.mainDoctorID == mDocID || mDocID == -1)
                {
                    Patient_Found_ID pffa = new Patient_Found_ID(p, abl);
                    pffa.Show();
                }
                else
                {
                    MessageBox.Show("The system could not find a single patient that matches the information you have entered!");
                }
            }
            else
            {
                 MessageBox.Show("No patient found..");
            }
        }

    }
}


