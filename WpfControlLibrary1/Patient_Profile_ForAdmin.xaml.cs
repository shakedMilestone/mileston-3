﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using Helper;
using BL_Backend;

namespace MCCSYS
{
    public partial class Patient_Profile_ForAdmin : Window
    {
        Ihelper helper;
        Patient curr;
        IBL abl;

        public Patient_Profile_ForAdmin(Patient currPat, IBL abl)
        {
            InitializeComponent();
            curr = currPat;
            this.abl = abl;
            helper = new HelperForPrint(abl);
            First.Text = currPat.firstName;
            Last.Text = currPat.lastName;
            Gender.Text = currPat.gender.ToString();
            Age.Text = currPat.age.ToString();
            Visits.Text = helper.DisplayVisitResult(currPat.visits);
            /*Visits.IsReadOnly = true;
            First.IsReadOnly = true;
            Last.IsReadOnly = true;
            Age.IsReadOnly = true;*/
            Gender.IsReadOnly = true;
        }

        private void update(object sender, RoutedEventArgs e)
        {
            abl.updatePatient(curr.ID, "age", Age.Text);
            abl.updatePatient(curr.ID, "first-name", First.Text);
            abl.updatePatient(curr.ID, "last-name", Last.Text);
            if (abl.findPatientByID(curr.ID).age != Convert.ToInt32(Age.Text))
                MessageBox.Show("Age hasn't changed!");
            MessageBox.Show("Patient updated!");
            this.Close();
        }

        private void remove(object sender, RoutedEventArgs e)
        {
            abl.remove('p', curr.ID);
            MessageBox.Show("Patient removed!");
            this.Close();
        }
    }
}
