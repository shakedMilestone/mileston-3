﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL;
using BL_Backend;

namespace Helper
{
    public interface Ihelper
    {
        /*void Run();*/
        string DisplayVisitResult(List<Visit> visits);
        string DisplayTreatmentResult(List<Treatment> treatments);
    }
}
