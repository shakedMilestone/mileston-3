﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;


namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for Visit_change.xaml
    /// </summary>
    public partial class Visit_change : Window
    {
     
        IBL bl;
        Patient curr;

        public Visit_change(Patient currPat, IBL abl)
        {
            curr = currPat;
           // helper = new HelperForPrint(abl);
            bl = abl;
            InitializeComponent();
            visitID.IsEnabled = true;
            Date.IsEnabled = false;
            Notes.IsEnabled = false;
        }
        
        private void update(object sender, RoutedEventArgs e)
        {
            String newdate = Date.Text;
            String newnotes = Notes.Text;
            bool date_check = bl.chkDate(newdate);
            if (date_check == true)
            {
                int id;
                int.TryParse(visitID.Text, out id);
                Visit v = bl.findVisitByID(id, curr);
                v.setDate(newdate);
                v.setNote(newnotes);
                MessageBox.Show("Updated!");
                this.Close();
            }
            else MessageBox.Show("Date should be DD/MM/YY");
        }

        private void search(object sender, RoutedEventArgs e)
        {
            Visit v;
            int id;
            int.TryParse(visitID.Text, out id);
            v = bl.findVisitByID(id, curr);
            if (v != null)
            {
                Date.Text = v.getVisitDate();
                Notes.Text = v.getVisitNote();
                Date.IsEnabled = true;
                Notes.IsEnabled = true;
                visitID.IsReadOnly = true;
                Date.IsEnabled = true;
                Notes.IsEnabled = true;
            }
            else MessageBox.Show("No such visit exist!");
        }

    }
}
