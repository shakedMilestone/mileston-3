﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;


namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for addNewDoctor.xaml
    /// </summary>
    public partial class addNewDoctor : Window
    {
    
        IBL aBL;

        public addNewDoctor(IBL abl)
        {
            InitializeComponent();
            this.aBL = abl;
            box_patient.IsEnabled = false;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            int id;
            int salary;
            char gender;
            if (Int32.TryParse(box_Id.Text, out id) && Int32.TryParse(box_salary.Text, out salary)
                && char.TryParse(box_gender.Text, out gender)) 
            {
                if (aBL.findDoctorByID(id) != null)
                {
                    MessageBox.Show("Doctor already exsist at the system");
                }

                else
                {
                    string first_name = box_firstName.Text;
                    string last_name = box_lastName.Text;
                    string patient_ID = box_patient.Text;
                    Doctor newDoctor = new Doctor(first_name, last_name, id, salary, gender, patient_ID);
                    aBL.add(newDoctor);
                    MessageBox.Show("Add new Doctor!");
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show ("error!!");
            }
        }
    }
}
