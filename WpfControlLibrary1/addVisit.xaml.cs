﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;
namespace MCCSYS
{

    public partial class addVisit : Window
    {
        IBL aBL;
        int docotorID;

        public addVisit(int doctorID, IBL aBL)
        {
            InitializeComponent();
            this.aBL = aBL;
            this.docotorID = doctorID;
            string patient = aBL.getPatientsList(doctorID);
            string[] patientList = patient.Split(',');
            foreach (string p in patientList)
            {
                ListBoxItem newItem = new ListBoxItem();
                newItem.Content = p;
                listOfPatient.Items.Add(newItem);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            string date = box_date.Text;
            string note = box_note.Text;
            if (aBL.chkDate(date))
            {
                string currItemIndex = ((ListBoxItem)listOfPatient.SelectedItem).Content.ToString();
                int currentPatientID = Convert.ToInt32(currItemIndex);
                Visit newVisit = new Visit(date, this.docotorID, currentPatientID, note);
                aBL.add(newVisit);
                MessageBox.Show("Add new Visit!");
                this.Close();
            }
            else MessageBox.Show("Date sould be DD/MM/YY");
        }
    }
}
