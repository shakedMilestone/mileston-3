﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for findVisit.xaml
    /// </summary>
    public partial class findVisit : Window
    {
        IBL aBL;
        int doctorID;
        int patIdChosen;

        public findVisit(int doctorID, IBL aBL)
        {
            InitializeComponent();
            this.doctorID = doctorID;
            this.aBL = aBL;
            string patient = aBL.getPatientsList(doctorID);
            string[] patientList = patient.Split(',');
            foreach (string p in patientList)
            {
                ListBoxItem newItem = new ListBoxItem();
                newItem.Content = p;
                listOfPatient.Items.Add(newItem);
            }
        }

        private void listOfPatient_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            listOfVisit.Items.Clear();
            string patientID = ((ListBoxItem)listOfPatient.SelectedItem).Content.ToString();
            patIdChosen = Convert.ToInt32(patientID);
            List<Visit> visit = aBL.getVisitBy("patient-id", patientID);
           
            foreach (Visit v in visit)
            {
                ListBoxItem newItem = new ListBoxItem();
                newItem.Content = v.visitID;
                listOfVisit.Items.Add(newItem);
            }
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            Patient p = aBL.findPatientByID(patIdChosen);
            int visit = Convert.ToInt32(((ListBoxItem)listOfVisit.SelectedItem).Content.ToString());
            Visit tmp = aBL.findVisitByID(visit, p);
            addNewTreatment add = new addNewTreatment(doctorID, visit, aBL,tmp);
            add.Show();
        }
    }
}
