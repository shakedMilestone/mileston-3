﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace MCCSYS
{

    public partial class addMenuForDoctor : Window
    {
        IBL aBL;
        int doctorID;

        public addMenuForDoctor(int id, IBL aBL)
        {
            InitializeComponent();
            this.doctorID = id;
            this.aBL = aBL;
        }

        private void button_addVisit_Click(object sender, RoutedEventArgs e)
        {
            addVisit visit = new addVisit(doctorID, aBL);
            visit.Show();
        }

        private void button_addTreatment_Click(object sender, RoutedEventArgs e)
        {
            findVisit fv = new findVisit(doctorID, aBL);
            fv.Show();
        }
    }
}
