﻿#pragma checksum "..\..\addNewDoctor.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E403E1102EDE673F0B2F9A281D976C73"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MCCSYS {
    
    
    /// <summary>
    /// addNewDoctor
    /// </summary>
    public partial class addNewDoctor : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 35 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_ID;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_FirstName;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_LastName;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_Patient;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_Salary;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_Gender;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox box_Id;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox box_firstName;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox box_lastName;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox box_patient;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox box_salary;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox box_gender;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\addNewDoctor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfControlLibrary1;component/addnewdoctor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\addNewDoctor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Text_ID = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 2:
            this.Text_FirstName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.Text_LastName = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.Text_Patient = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.Text_Salary = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.Text_Gender = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.box_Id = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.box_firstName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.box_lastName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.box_patient = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.box_salary = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.box_gender = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.addButton = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\addNewDoctor.xaml"
            this.addButton.Click += new System.Windows.RoutedEventHandler(this.addButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

