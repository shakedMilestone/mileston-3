﻿#pragma checksum "..\..\Doctor_search.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6A241F94FF15A518A4414318FBE7AFC4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace MCCSYS {
    
    
    /// <summary>
    /// Doctor_search
    /// </summary>
    public partial class Doctor_search : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton First;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Last;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton Gender;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox FirstText;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LastText;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox GenderText;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton ID;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\Doctor_search.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox IDText;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfControlLibrary1;component/doctor_search.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Doctor_search.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.First = ((System.Windows.Controls.RadioButton)(target));
            
            #line 10 "..\..\Doctor_search.xaml"
            this.First.Checked += new System.Windows.RoutedEventHandler(this.First_Checked);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Last = ((System.Windows.Controls.RadioButton)(target));
            
            #line 11 "..\..\Doctor_search.xaml"
            this.Last.Checked += new System.Windows.RoutedEventHandler(this.Last_Checked);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Gender = ((System.Windows.Controls.RadioButton)(target));
            
            #line 12 "..\..\Doctor_search.xaml"
            this.Gender.Checked += new System.Windows.RoutedEventHandler(this.Gender_Checked);
            
            #line default
            #line hidden
            return;
            case 4:
            this.FirstText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.LastText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.GenderText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.ID = ((System.Windows.Controls.RadioButton)(target));
            
            #line 16 "..\..\Doctor_search.xaml"
            this.ID.Checked += new System.Windows.RoutedEventHandler(this.ID_Checked);
            
            #line default
            #line hidden
            return;
            case 8:
            this.IDText = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            
            #line 21 "..\..\Doctor_search.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.search);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

