﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;


namespace MCCSYS
{
    public partial class welcome_admin : Window
    {

        IBL aBL;
        LoginUser user;

        public welcome_admin(IBL aBL, LoginUser user)
        {
            InitializeComponent();
            this.aBL = aBL;
            this.user = user;
        }

        private void setting_Click(object sender, RoutedEventArgs e)
        {
            Settings set = new Settings(user, aBL);
            set.Show();
        }

        private void search_Doc(object sender, RoutedEventArgs e)
        {
            Doctor_search ds = new Doctor_search(aBL);
            ds.Show();
        }


        private void addDoc_Click(object sender, RoutedEventArgs e)
        {
            addNewDoctor addDoctor = new addNewDoctor(aBL);
            addDoctor.Show();
        }

        private void addPatient_Click(object sender, RoutedEventArgs e)
        {
            addNewPatient addPatient = new addNewPatient(aBL);
            addPatient.Show();
        }

        private void search_pat(object sender, RoutedEventArgs e)
        {
            Patient_Search ps = new Patient_Search(aBL, -1);
            ps.Show();
        }
    }
}
