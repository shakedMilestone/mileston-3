﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;

namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for addNewTreatment.xaml
    /// </summary>
    public partial class addNewTreatment : Window
    {
        IBL aBL;
        int docotorID;
        int visitID;

        public addNewTreatment(int doctorID, int visitID, IBL aBL, Visit tmp)
        {
            InitializeComponent();
            this.visitID = visitID;
            this.docotorID = doctorID;
            this.aBL = aBL;
            box_start.Text = tmp.getVisitDate().ToString();
            box_start.IsReadOnly = true;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            string start = box_start.Text;
            string end = box_end.Text;
            if (aBL.chkDate(start) && aBL.chkDate(end))
            {
                string prognosis = box_prognosis.Text;
                string prescription = box_prescription.Text;
                Treatment newTreatment = new Treatment(start, end, docotorID, prognosis, prescription, visitID);
                aBL.add(newTreatment);
                MessageBox.Show("a new Treatment was added!");
                this.Close();
            }
            else MessageBox.Show("Date sould be DD/MM/YY");
        }
    }
}
