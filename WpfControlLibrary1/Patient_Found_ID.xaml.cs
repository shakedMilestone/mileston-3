﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using Helper;
using BL_Backend;


namespace MCCSYS
{
    public partial class Patient_Found_ID : Window
    {
        Ihelper helper;
        IBL bl;
        Patient curr;

        public Patient_Found_ID(Patient currPat, IBL abl)
        {
            InitializeComponent();
            helper = new HelperForPrint(abl);
            bl = abl;
            curr = currPat;
            First.Text = currPat.firstName;
            Last.Text = currPat.lastName;
            Gender.Text = currPat.gender.ToString();
            Age.Text = currPat.age.ToString();
            Visits.Text = helper.DisplayVisitResult(currPat.visits);
            Visits.IsReadOnly = true;
            Gender.IsReadOnly = true;
        }

        private void update(object sender, RoutedEventArgs e)
        {
            bl.updatePatient(curr.ID, "age", Age.Text);
            bl.updatePatient(curr.ID, "first-name", First.Text);
            bl.updatePatient(curr.ID, "last-name", Last.Text);
            if (bl.findPatientByID(curr.ID).age != Convert.ToInt32(Age.Text))
                MessageBox.Show("Age hasn't changed!");
            MessageBox.Show("Patient updated!");
            this.Close();
        }

        private void remove(object sender, RoutedEventArgs e)
        {
            bl.remove('p', curr.ID);
            MessageBox.Show("Patient removed!");
            this.Close();
        }

        private void update_visit(object sender, RoutedEventArgs e)
        {
            Visit_change vc = new Visit_change(curr, bl);
            vc.Show();
            Visits.Text = helper.DisplayVisitResult(curr.visits);
            Visits.UpdateLayout();
        }

        private void update_treatment(object sender, RoutedEventArgs e)
        {
            Treatment_change tc = new Treatment_change(curr, bl);
            tc.Show();
        }
    }
}