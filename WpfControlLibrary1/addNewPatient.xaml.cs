﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using BL_Backend;



namespace MCCSYS
{
    /// <summary>
    /// Interaction logic for addNewPatient.xaml
    /// </summary>
    public partial class addNewPatient : Window
    {
        IBL aBL;
        public addNewPatient(IBL bl)
        {
            InitializeComponent();
            aBL = bl;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            int id;
            int doctorID;  
            char gender;
            int age;
            if (Int32.TryParse(box_Id.Text, out id) && Int32.TryParse(box_Age.Text, out age) &&
                Int32.TryParse(box_MainDoc.Text, out doctorID) && char.TryParse(box_gender.Text, out gender))
            {
                if (aBL.findPatientByID(id) != null)
                {
                    MessageBox.Show("Patient already exsist at the system");
                }
                else
                {
                    string first_name = box_firstName.Text;
                    string last_name = box_lastName.Text;
                    Patient newPatient = new Patient(first_name, last_name, id, doctorID, gender, age);
                    aBL.add(newPatient);
                    MessageBox.Show("Add new patient!");
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("error!");
            }
        }
    }
}
