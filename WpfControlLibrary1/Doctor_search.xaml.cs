﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Helper;
using BL;
using BL_Backend;

namespace MCCSYS
{

    public partial class Doctor_search : Window
    {
        Ihelper helper;
        IBL abl;

        public Doctor_search(IBL mainBL)
        {
            helper = new HelperForPrint(mainBL);
            abl = mainBL;
            InitializeComponent();
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void Gender_Checked(object sender, RoutedEventArgs e)
        {
            GenderText.IsEnabled = true;
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
        }

        private void Last_Checked(object sender, RoutedEventArgs e)
        {
            LastText.IsEnabled = true;
            FirstText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void First_Checked(object sender, RoutedEventArgs e)
        {
            FirstText.IsEnabled = true;
            LastText.IsEnabled = false;
            IDText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void ID_Checked(object sender, RoutedEventArgs e)
        {
            IDText.IsEnabled = true;
            FirstText.IsEnabled = false;
            LastText.IsEnabled = false;
            GenderText.IsEnabled = false;
        }

        private void search(object sender, RoutedEventArgs e)
        {
            if (Gender.IsChecked == true)
            {
                string genderSearch = GenderText.Text;
                if (genderSearch != "")
                    searchNotID(genderSearch, "gender");
                else MessageBox.Show("No data was entered!");
            }
            if (First.IsChecked == true)
            {
                string firstSearch = FirstText.Text;
                if (firstSearch != "")
                    searchNotID(firstSearch, "first-name");
                else MessageBox.Show("No data was entered!");
            }
            if (Last.IsChecked == true)
            {
                string lastSearch = LastText.Text;
                if (lastSearch != "")
                    searchNotID(lastSearch, "last-name");
                else MessageBox.Show("No data was entered!");
            }
            if (ID.IsChecked == true)
            {
                string idSearch = IDText.Text;
                searchID(idSearch);
            }
        }

        public void searchNotID(string info, string catagory)
        {
            string str="";
            List<Doctor> doctors = abl.getDoctorBy(catagory, info); ;
            foreach (Doctor doc in doctors)
            {
                    str += doc + "\n";
            }
            Doctor_found dFound = new Doctor_found(str);
            dFound.Show();
        }
        
        public void searchID(string id)
        {
            int idvalue;
            int.TryParse(id,out idvalue);
            if (abl.findDoctorByID(idvalue) != null)
            {
                Doctor d = abl.findDoctorByID(idvalue);
                if (d != null)
                {
                    Doctor_Found_ID dfid = new Doctor_Found_ID(d, abl);
                    dfid.Show();
                }
                else
                {
                    MessageBox.Show("The system could not find a single doctor that matches the information you have entered!");
                }
            }
            else
            {
                MessageBox.Show("No doctor found..");
            }
        }

    }
}


