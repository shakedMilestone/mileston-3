﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BL;
using Helper;
using BL_Backend;

namespace MCCSYS
{
    public partial class Patient_Profile : Window
    {
        Ihelper helper;
        LoginUser curr;
        IBL abl;

        public Patient_Profile(LoginUser lu, Patient currPat, IBL abl)
        {
            InitializeComponent();
            this.abl = abl;
            helper = new HelperForPrint(abl);
            curr = lu;
            First.Text = currPat.firstName;
            Last.Text = currPat.lastName;
            Gender.Text = currPat.gender.ToString();
            Age.Text = currPat.age.ToString();
            Visits.Text = helper.DisplayVisitResult(currPat.visits);
            Visits.IsReadOnly = true;
            First.IsReadOnly = true;
            Last.IsReadOnly = true;
            Age.IsReadOnly = true;
            Gender.IsReadOnly = true;
        }

        private void settings(object sender, RoutedEventArgs e)
        {
            Settings settings = new Settings(curr,abl);
            settings.Show();
        }
    }
}
