﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Doctor
    {
        public int ID;
        public String firstName;
        public String lastName;
        public String patientID;
        public double salary;
        char gender;
    
		public override string ToString(){
					String ans = "";
					ans += "DocID: " + ID;
					ans += "\nDoc's name: " + firstName+" " + lastName;
                    ans += "\nDoc's patients: " + patientID;
                    ans += "\nDoc's salary: " + salary;
					ans += "\nDoc's gender: " + gender + "\n";
					return ans;
		}

        public char getGender()
        {
            return gender;
        }

        public Doctor(String _firstName, String _lastName,int _ID, double _salary, char _gender, String _patientID){
    
            firstName= _firstName;
            lastName= _lastName;
            salary= _salary;
            gender=_gender;
            patientID = _patientID;
            ID = _ID;
        }
    }


}

