﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Treatment
    {
        public String dateOfStart;
        public String dateOfFinish;
        public int createdByDoctor; //doctro'ID who assigned this treatmnt
        public String Prognosis;
        public String Prescriptions;
        public int treatmentId;
        public int visitID;

    public Treatment(String _dateOfStart, String _dateOfFinish, int _DoctorID, 
        String _Prognosis, String _Prescriptions, int _visitID)
    {
        if (_dateOfStart != null & _dateOfFinish != null & _DoctorID != 0 & _Prescriptions != null)
        {
            dateOfStart = _dateOfStart;
            dateOfFinish = _dateOfFinish;
            createdByDoctor = _DoctorID;
            Prognosis = _Prognosis;
            Prescriptions = _Prescriptions;
            visitID = _visitID;
        }
    }

    public int getTreatmentID()
    {
        return treatmentId;
    }
	
	public override String ToString(){
            String ans = "";
            ans += "Treatment's ID: " + treatmentId;
            ans += "\nTreatment's start date: " + dateOfStart;
            ans += "\nTreatment's end date: " + dateOfFinish;
            ans += "\nTreatment was given by : " + createdByDoctor;
            ans += "\nTreatment's Prognosis: " + Prognosis;
            ans += "\nPrescriptions: " + Prescriptions+"\n";
            return ans;
        }




}
}

