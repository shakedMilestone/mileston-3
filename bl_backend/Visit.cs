﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Visit
    {
        public int visitID;
        public String dateOfVisit;
        public int assignedDoctor;
        public int patientID;
        public String DoctorNotes;
        public List<Treatment> listofTreatments;

        public int getVisitId()
        {
            return visitID;
        }

        public void setDate(String date)
        {
            dateOfVisit = date;
        }

        public void setNote(String note)
        {
            DoctorNotes = note;
        }

		public int getListSize()
        {
            int sizeOfList;
            sizeOfList = listofTreatments.Count;
            return sizeOfList;
        }

        public override string ToString(){
            String ans = "";
            ans += "Visit's ID: " + visitID;
            ans += "\nVisit's date: " + dateOfVisit;
            ans += "\nVisit assigned doctor : " + assignedDoctor;
            ans += "\nVisiting patient: " + patientID;
            ans += "\nDoctors notes: " + DoctorNotes+"\n";
            try
            {
                if (getListSize() > 0)
                {
                    ans += "\nList of treatments: \n";
                    for (int i = 0; i < getListSize(); i++)
                    {
                        ans += listofTreatments.ElementAt(i).ToString() + " ******* \n";
                    }
                }
            }
            catch
            {
                return ans;
            }
            return ans;
        }

        public string getVisitDate()
        {
            return dateOfVisit;
        }

        public string getVisitNote()
        {
            return DoctorNotes;
        }

        public Visit(String _dateOfVisit, int _assignedDoctor, int _patientID, String _DoctorNotes)
        {
            dateOfVisit = _dateOfVisit;
            assignedDoctor = _assignedDoctor;
            patientID = _patientID;
            DoctorNotes = _DoctorNotes;
            listofTreatments = new List<Treatment>();
        }
    }
}

