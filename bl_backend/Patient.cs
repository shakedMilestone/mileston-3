﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class Patient
    {
        public int ID;
        public String firstName;
        public String lastName;
        public int mainDoctorID;   //Doctr's ID
        public List<Visit> visits = new List<Visit>();
        public char gender;
        public int age;

        public Patient(String _firstName, String _lastName,int _ID, int _mainDoctorID, char _gender, int _age)
        {
            firstName = _firstName;
            lastName = _lastName;
            mainDoctorID = _mainDoctorID;
            visits = new List<Visit>();
            gender = _gender;
            age = _age;
            ID = _ID;
        }
		
		 public override String ToString(){
            String ans = "";
            ans += "Pat's ID: " + ID;
            ans += "\nPat's name: " + firstName + " " + lastName;
            ans += "\nPat's main Doc: " + mainDoctorID;
            ans += "\nPat's age: " + age;
            ans += "\nPat's gender: " + gender+"\n";
            return ans;
        }

    }
}

