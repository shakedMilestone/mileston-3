﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL_Backend
{
    public class LoginUser
    {
        private string username;
        private string password;
        private char type;
        private int Id;
        // need to create a connection with each doctor/patient

        public LoginUser(string usr, string pass, char userType, int id)
        {
            this.username = usr;
            this.password = pass;
            this.type = userType;
            this.Id = id;
        }

        public void setPass(string newpass)
        {
            password = newpass;
        }

        public string  getUsrName(){
            return username;
        }

        public int getId()
        {
            return Id;
        }

        public string getpass()
        {
            return password;
        }

        public char getUsrType()
        {
            return type;
        }

    }
}
