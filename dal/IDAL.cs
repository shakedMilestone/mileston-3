﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace DAL
{
    public interface IDAL
    {
        void add(Doctor d);
        void add(Patient p);
        void add(Visit v);
        void add(Treatment t);
        void removeDoctor(int ID);
        void removePatient(int ID);
        void updateDoctor(int doctorID, String field, String updateField);
        void updatePatient(int patientID, String field, String updateField);
        /**
         * quaries that must be impliment! 
         * */
        Patient findPatientByID(int PatId);         //returns what?
        Doctor findDoctorByID(int DocId);           //returns what?
        Visit findVisit(int visitID,String startDate);
        List<Doctor> getDoctorBy(String catagory, String info);
        //  {main doctor, age, gender}
        List<Patient> getPatientBy(String catagory, String info);
        List<Visit> getVisitBy(String catagory, String info);
        Treatment findTreatmentByID(int id, Visit v);
        Visit findVisitByID(int id, Patient p);
        List<Treatment> getTreatmentBy(String catagory, String info);
        String getPatientsList(int doctorID);
        List<Visit> visitPerPatient(int PatientID);
        List<Treatment> treatListPerVisit(int visitID);
        LoginUser findUser(string usr);
        void changePass(LoginUser currUser, string pass);
    }
}