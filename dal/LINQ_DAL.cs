﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL_Backend;

namespace DAL
{
    public class LINQ_DAL : IDAL
    {
        public List<Patient> DBOfPatient;
        public IList<Doctor> DBOfDoctors;
        public List<LoginUser> DBofUsers;
        public int countVisits;
        public int countTreatments;

        public LINQ_DAL()
        {
            DBOfPatient = new List<Patient>();
            DBOfDoctors = new List<Doctor>();
            DBofUsers = new List<LoginUser>();
            initDataBases();
        }



        public void initDataBases()
        {
            Doctor Noam = new Doctor("Noam", "Tomasis", 123, 100000, 'F', "");
            Doctor Hen = new Doctor("Hen", "Damri", 234, 999999, 'F', "");
            Doctor israelDoc = new Doctor("Israel", "Doc", 789, 150000, 'M', "");
            add(Noam);
            add(Hen);
            add(israelDoc);
            Patient Yair = new Patient("Yaya", "Tomasis", 321, 123, 'M', 12);
            Patient Alon = new Patient("Alon", "Damri", 432, 123, 'M', 14);
            Patient Sapir = new Patient("Sapir", "Damri", 543, 234, 'F', 16);
            Patient israelPat = new Patient("Israel", "Pat", 777, 789, 'M', 34);
            Patient Moshe = new Patient("Moshe", "Haiun", 876, 789, 'M', 25);
            Patient HenA = new Patient("Hen", "Aharoni", 654, 789, 'F', 24);
            Patient Innon = new Patient("Innon", "Sagron", 765, 789, 'M', 25);
            add(israelPat);
            add(Yair);
            add(Alon);
            add(Sapir);
            add(Moshe);
            add(HenA);
            add(Innon);
            LoginUser NoamUSR = new LoginUser("noam", "1234",'d',123);
            LoginUser HenUSR = new LoginUser("hen", "hen",'d',234);
            LoginUser YairUSR = new LoginUser("yair", "yaya",'p',321);
            LoginUser AlonUSR = new LoginUser("alon", "alon",'p',432);
            LoginUser SapirUSR = new LoginUser("sapir", "damri",'p',543);
            LoginUser MoshUsr = new LoginUser("mosh", "1234", 'p', 876);
            LoginUser HenaUSR = new LoginUser("hen", "buba", 'p', 654);
            LoginUser InnonUSR = new LoginUser("innon", "p", 'p', 765);
            LoginUser admin = new LoginUser("admin", "admin",'a',000);
            LoginUser israeladminUSR = new LoginUser("1", "p", 'a', 111);
            LoginUser israelPatUSR = new LoginUser("3", "p", 'p', 777);
            LoginUser israelDocUSR = new LoginUser("2", "p", 'd', 789);
            add(NoamUSR);
            add(HenUSR);
            add(YairUSR);
            add(AlonUSR);
            add(SapirUSR);
            add(MoshUsr);
            add(HenaUSR);
            add(InnonUSR);
            add(israelDocUSR);
            add(israelPatUSR);
            add(israeladminUSR);
            add(admin);
            Visit v1 = new Visit("02/05/13",234,432,"Fever");
            add(v1);
            Treatment t1 = new Treatment("02/05/13", "12/05/13", 234, "Allergys", "Allergy shot", v1.getVisitId());
            add(t1);
            Visit v2 = new Visit("12/12/14", 123, 321, "Hand boo boo");
            add(v2);
            Treatment t2 = new Treatment("12/12/14", "16/12/14", 123, "Hand boo boo", "Band aid", v2.getVisitId());
            add(t2);
            Visit v3 = new Visit("01/05/14", 789, 777, "Fever");
            add(v3);
            Treatment t3 = new Treatment("01/05/14", "10/05/14", 789, "Allergys", "Allergy shot", v3.getVisitId());
            add(t3);
        }

        public void changePass(LoginUser currUser, string pass)
        {
            currUser.setPass(pass);
        }

        public Treatment findTreatmentByID(int id, Visit v)
        {
            int i = 0;
            int size = v.listofTreatments.Count;
            for (; i < size; i++)
            {
                if (id == v.listofTreatments.ElementAt(i).getTreatmentID())
                {
                    return v.listofTreatments.ElementAt(i);
                }
            }
            return null;
        }

        public Visit findVisitByID(int id, Patient p)
        {
            int i = 0;
            int size = p.visits.Count;
            for (; i < size; i++)
            {
                if (id == p.visits.ElementAt(i).getVisitId())
                {
                    return p.visits.ElementAt(i);
                }
            }
            return null;
        }

        public LoginUser findUser(string usr)
        {
            foreach (LoginUser currentUser in DBofUsers)
            {
                if (currentUser.getUsrName() == usr)
                {
                    return currentUser;
                }
            }
            return null;
        }

     
        public void add(Doctor d)
        {
            DBOfDoctors.Add(d);
        }

        public void add(LoginUser l)
        {
            if (l != null)
                DBofUsers.Add(l);
        }

        public void add(Patient p)
        {
            Doctor doctor = findDoctorByID(p.mainDoctorID);
            if (doctor != null)
            {
                DBOfPatient.Add(p);
                addPatientToDoctorList(p);
            }
        }

        public void add(Visit v)
        {
            Patient p = findPatientByID(v.patientID);
            int i = 0;
            for(;i<DBOfPatient.Count;i++){
                countVisits += DBOfPatient.ElementAt(i).visits.Count;
            }
            v.visitID = countVisits + 1;
            p.visits.Add(v);
        }

        public void add(Treatment t)
        {
            String date = t.dateOfStart;
            Visit visit = findVisit(t.visitID,date);
            if (visit != null)
            {
                Patient p = findPatientByID(visit.patientID);
                int j = 0;
                for (; j < p.visits.Count; j++)
                {
                    if (visit != null)
                    {
                        int i = 0;
                        for (; i < visit.listofTreatments.Count; i++)
                        {
                            countTreatments += DBOfPatient.ElementAt(j).visits.ElementAt(i).listofTreatments.Count;
                        }
                    }
                }
                countTreatments++;
                t.treatmentId = countTreatments;
                visit.listofTreatments.Add(t);
            }
        }

        private void addPatientToDoctorList(Patient p)
        {
            Doctor doctor = findDoctorByID(p.mainDoctorID);
            if (!doctor.patientID.Contains(Convert.ToString(p.ID)))
            {
                if (doctor.patientID != "")
                    doctor.patientID += "," + p.ID;
                else
                    doctor.patientID += p.ID;
            }
        }

        public void removeDoctor(int ID)
        {
            DBOfDoctors.Remove(findDoctorByID(ID));
        }

        public void removePatient(int ID)
        {
            removePatintFromDoctorList(ID);
            DBOfPatient.Remove(findPatientByID(ID));
        }

        private void removePatintFromDoctorList(int patientID)
        {
            foreach (Doctor doctor in DBOfDoctors)
            {
                if (doctor.patientID.Contains(Convert.ToString(patientID)))
                {
                    removeFromList(doctor, patientID);
                    return;
                }
            }
        }

        private void removeFromList(Doctor doctor, int patientID)
        {
           String strPatientID= Convert.ToString(patientID);
           String stringPatientId= strPatientID+  ","  ;
           String laststringPatientId = "," + strPatientID;
          
            //if there are more than 1 patient 
           if (doctor.patientID.Contains(stringPatientId))
            {
               doctor.patientID= 
                   doctor.patientID.Replace(stringPatientId, "");
            }
            //in case of singal id
            if (doctor.patientID.Contains (strPatientID))
            {
                doctor.patientID = 
                    doctor.patientID.Replace(Convert.ToString(patientID), "");
            }
            if (doctor.patientID.EndsWith(","))
            {
                doctor.patientID=
                    doctor.patientID.Substring (0,doctor.patientID.Length-1);
            }
        }

        public void updateDoctor(int doctorID, String field, String updateField)
        {
            Doctor doctor = findDoctorByID(doctorID);
            switch (field)
            {
                case "first-name":
                    doctor.firstName = updateField;
                    break;

                case "last-name":
                    doctor.lastName = updateField;
                    break;

                case "salary":
                    doctor.salary = Convert.ToDouble(updateField);
                    break;

                default:
                    //error!!!
                    break;
            }
        }

        public void updatePatient(int patientID, String field, String updateField)
        {
            Patient patient = findPatientByID(patientID);
            switch (field)
            {
                case "first-name":
                    patient.firstName = updateField;
                    break;
                case "last-name":
                    patient.lastName = updateField;
                    break;
                case "main-doc-id":
                    patient.mainDoctorID = Convert.ToInt32(updateField);
                    break;
                case "age":
                    if (Convert.ToInt32(updateField) <= 100)
                        patient.age = Convert.ToInt32(updateField);
                    break;
                case "gender":
                    patient.gender = updateField[0];
                    break;
                default:
                    //error!!!
                    break;
            }
        }

        /**
         * quaries that must be impliment! 
         * */

        public Patient findPatientByID(int PatId)
        {
            foreach (Patient currentPatient in DBOfPatient)
            {
                if (currentPatient.ID == PatId)
                {
                    return currentPatient;
                }
            }
            return null;
        }


        public Doctor findDoctorByID(int DocId)
        {
            foreach (Doctor currentDoctor in DBOfDoctors)
            {
                if (currentDoctor.ID == DocId)
                {
                    /*Console.Write("this is the docs tostring inside DAL ");
                    Console.Write(currentDoctor);*/
                    return currentDoctor;
                }
            }
            return null;
        }

        //return main doctor of specific patient
        public Visit findVisit(int visitID,String startdate)
        {
            foreach (Patient patient in DBOfPatient)
            {
                foreach (Visit visit in patient.visits)
                {
                    if (visit.visitID == visitID && visit.dateOfVisit==startdate)
                        return visit;
                }
            }
            return null;
        }


        /**
     * Optional quaries 
       **/

        public String getPatientsList(int doctorID)
        {
            foreach (Doctor doctor in DBOfDoctors)
            {
                if (doctor.ID == doctorID)
                {
                    return doctor.patientID;
                }
            }
            return null;
        }


        public List<Visit> visitPerPatient(int PatientID)
        {
            foreach (Patient patient in DBOfPatient)
            {
                if (patient.ID == PatientID)
                    return patient.visits;
            }
            return null;
        }



        public List<Treatment> treatListPerVisit(int visitID)
        {
            foreach (Patient patient in DBOfPatient)
            {
                foreach (Visit visit in patient.visits)
                {
                    if (visit.visitID == visitID)
                        return visit.listofTreatments;
                }
            }
            return null;
        }



        public List<Visit> getVisitBy(String catagory, String info)
        {
            switch (catagory)
            {
                case "patient-id":
                    {
                        return findPatientByID(Convert.ToInt32(info)).visits;
                    }
                case "date-of-visit":
                    {
                        var resault = from patient in DBOfPatient
                                      from visit in patient.visits
                                      where visit.dateOfVisit == info
                                      select visit;
                        return resault.ToList();
                    }
                default:
                    {
                        return new List<Visit>();
                    }
            }
        }

        public List<Treatment> getTreatmentBy(String catagory, String info)
        {
            switch (catagory)
            {
                case "start-date":
                    {
                        var resault = from patient in DBOfPatient
                                      from visit in patient.visits
                                      from treatment in visit.listofTreatments
                                      where treatment.dateOfStart == info
                                      select treatment;

                        return resault.ToList();
                    }
                case "assigned-doctor-id":
                    {
                        var resault = from patient in DBOfPatient
                                      from visit in patient.visits
                                      from treatment in visit.listofTreatments
                                      where treatment.createdByDoctor == Convert.ToInt32(info)
                                      select treatment;
                        return resault.ToList();
                    }
                default:
                    return new List<Treatment>();
            }
        }

        public List<Doctor> getDoctorBy(String catagory, String info)
        {
            switch (catagory)
            {
                case "first-name":
                    {
                        var resault = from doctor in DBOfDoctors
                                      where doctor.firstName == info
                                      select doctor;

                        return resault.ToList();
                    }
                case "last-name":
                    {
                        var resault = from doctor in DBOfDoctors
                                      where doctor.lastName == info
                                      select doctor;

                        return resault.ToList();
                    }
                case "gender":
                    {
                        var resault = from doctor in DBOfDoctors
                                      where doctor.getGender() == info[0]
                                      select doctor;

                        return resault.ToList();
                    }

                default:
                    return new List<Doctor>();
            }
        }

        //  {main doctor, age, gender}
        public List<Patient> getPatientBy(String catagory, String info)
        {
            switch (catagory)
            {

                case "first-name":
                    {
                        var resault = from patient in DBOfPatient
                                      where patient.firstName == info
                                      select patient;

                        return resault.ToList();
                    }
                case "last-name":
                    {
                        var resault = from patient in DBOfPatient
                                      where patient.lastName == info
                                      select patient;

                        return resault.ToList();
                    }
                case "age":
                    {
                        var resault = from patient in DBOfPatient
                                      where patient.age == Convert.ToInt32(info)
                                      select patient;
                        return resault.ToList();

                    }
                case "gender":
                    {
                        var resault = from patient in DBOfPatient
                                      where patient.gender == Convert.ToChar(info)
                                      select patient;

                        return resault.ToList();
                    }

                default:
                    return new List<Patient>();
            }
        }
    }
}